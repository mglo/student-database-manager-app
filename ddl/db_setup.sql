--
-- Baza danych: `universitydb` creted on https://www.db4free.net
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tblCourses`
--

CREATE TABLE `tblCourses` (
  `courseID` int(11) NOT NULL,
  `courseName` VARCHAR(45) NOT NULL,
  `coursePrice` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tblPayments`
--

CREATE TABLE `tblPayments` (
  `studentID` int(11) NOT NULL,
  `paymentID` int(11) NOT NULL,
  `paymentValue` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tblStudentCourses`
--

CREATE TABLE `tblStudentCourses` (
  `studentID` int(11) NOT NULL,
  `courseID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tblStudents`
--

CREATE TABLE `tblStudents` (
  `studentID` int(11) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `displayName` varchar(100) NOT NULL,
  `gradeYear` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `tblCourses`
--
ALTER TABLE `tblCourses`
  ADD PRIMARY KEY (`courseID`);

--
-- Indeksy dla tabeli `tblStudents`
--
ALTER TABLE `tblStudents`
  ADD PRIMARY KEY (`studentID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `tblCourses`
--
ALTER TABLE `tblCourses`
  MODIFY `courseID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `tblStudents`
--
ALTER TABLE `tblStudents`
  MODIFY `studentID` int(11) NOT NULL AUTO_INCREMENT;