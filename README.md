# Student Database Manager - JDBC / MySQL

Application for manage recruitment of students at Universities

### Prerequisites

Java 8, 
MySQL database - DB setup included in ddl folder
Credentials - credentials stored in ddl folder in DBCredentials.csv folder in following format: jdbc:mysql://db4free.net:3306/universitydb?useSSL=false;user;password

### GUI

https://gitlab.com/mglo/student-database-manager-app/wikis/uploads/b0b33c9ad1f7ee7d258e036f6ddb9f26/Student_DB_App.PNG

## Authors

* **Mateusz Głowa**
