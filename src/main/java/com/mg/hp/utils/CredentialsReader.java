package com.mg.hp.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CredentialsReader {
    //TODO implement Builder pattern

    public static final String CREDENTIALS_FILE = "ddl/DBCredentials.csv";
    public static final String SEPARATOR = ";";

    public static String getURL(){
        String line = "";

        try(BufferedReader br = new BufferedReader(new FileReader(CREDENTIALS_FILE))) {

            while((line = br.readLine()) != null){

                String[] data = line.split(SEPARATOR);

                return data[0];
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return "";
    }

    public static String getPass(){
        String line = "";
        String separator = ";";

        try(BufferedReader br = new BufferedReader(new FileReader(CREDENTIALS_FILE))) {

            while((line = br.readLine()) != null){

                String[] data = line.split(SEPARATOR);

                return data[2];
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return "";
    }

    public static String getUser(){
        String line = "";


        try(BufferedReader br = new BufferedReader(new FileReader(CREDENTIALS_FILE))) {

            while((line = br.readLine()) != null){

                String[] data = line.split(SEPARATOR);

                return data[1];
            }
        }catch(IOException ex){
            ex.printStackTrace();
        }

        return "";
    }
}
