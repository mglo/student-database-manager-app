package com.mg.hp.gui;

import com.mg.hp.studentDBapp.Student;
import com.mg.hp.studentDBapp.StudentDAO;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//ADD student GUI
public class AddNewStudentGui {

    JTextField firstNameTextField, lastNameTextField, gradeYearTextField;
    StudentDAO studentDAO = new StudentDAO();


    public AddNewStudentGui() {
        JFrame frame = new JFrame("Add Student Profile");
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        JPanel mainPanel = new JPanel();

        JLabel firstNameLabel = new JLabel("First Name");
        final JLabel lastNameLabel = new JLabel("Last Name");
        JLabel gradeYearLabel = new JLabel("gradeYear");
        final JLabel statusLabel = new JLabel();

        firstNameTextField = new JTextField();
        lastNameTextField = new JTextField();
        gradeYearTextField = new JTextField();

        JButton addButton = new JButton("Add");

        mainPanel.setSize(500, 500);
        GroupLayout layout = new GroupLayout(mainPanel);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        mainPanel.setLayout(layout);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(firstNameLabel)
                    .addComponent(lastNameLabel)
                    .addComponent(gradeYearLabel)
                    .addComponent(statusLabel))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                    .addComponent(firstNameTextField, 150, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lastNameTextField, 150, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(gradeYearTextField, 150, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(addButton))
        );

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(firstNameLabel)
                .addComponent(firstNameTextField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lastNameLabel)
                .addComponent(lastNameTextField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(gradeYearLabel)
                .addComponent(gradeYearTextField))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(statusLabel)
                .addComponent(addButton))
        );

        frame.add(mainPanel);
        frame.pack();
        frame.setVisible(true);

        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               studentDAO.addStudent(new Student(firstNameTextField.getText(), lastNameTextField.getText(), Integer.valueOf(gradeYearTextField.getText())));
                statusLabel.setText("Student Added.");
               //System.out.println(firstNameTextField.getText() + "Searching for student by usage of DAO object");
            }
        });
    }





}
