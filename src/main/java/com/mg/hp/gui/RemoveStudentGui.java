package com.mg.hp.gui;

import javax.swing.*;

public class RemoveStudentGui {

    public RemoveStudentGui() {
        JFrame frame = new JFrame("Remove Student");
        JPanel panel = new JPanel();

        JLabel searchLabel = new JLabel("Student Number");
        JTextField searchTextField = new JTextField();
        searchTextField = new JTextField();
        JButton searchButton = new JButton("Remove");

        panel.setSize(500, 500);
        GroupLayout layout = new GroupLayout(panel);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        panel.setLayout(layout);


        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addComponent(searchLabel)
                .addComponent(searchTextField, 100, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup()
                                .addComponent(searchButton))

                );



        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(searchLabel)
                                .addComponent(searchTextField)
                                .addComponent(searchButton))

        );


        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }
}
