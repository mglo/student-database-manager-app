package com.mg.hp.gui;

import com.mg.hp.studentDBapp.Student;
import com.mg.hp.studentDBapp.StudentDAO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class AllStudentsGui {

    JTable table;
    DefaultTableModel dtm;
    String[] columnNames;
    final StudentDAO studentDAO;

    public AllStudentsGui() {

        studentDAO = new StudentDAO();
        JFrame frame = new JFrame("Student Profile");
        frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        JPanel mainPanel = new JPanel();

        columnNames = new String[]{"First Name",
                "Last Name",
                "Courses",
                "GradeYear",
                "Balance"};

        final Object[][] data = {
                {"Kathy", "Smith",
                        "Snowboarding", 5, 000}
        };


        JButton searchButton = new JButton("Refresh");

        dtm = new DefaultTableModel(data,columnNames);
        table = new JTable(dtm);

        mainPanel.setSize(500, 500);
        GroupLayout layout = new GroupLayout(mainPanel);
        layout.setAutoCreateContainerGaps(true);
        layout.setAutoCreateGaps(true);
        mainPanel.setLayout(layout);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
                        .addComponent(table.getTableHeader())
                        .addComponent(table))
                .addGroup(layout.createParallelGroup()
                        .addComponent(searchButton))
        );

        layout.setVerticalGroup(
                layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(table.getTableHeader())
                                .addComponent(table)
                                .addComponent(searchButton))
        );

        frame.add(mainPanel);
        frame.pack();
        frame.setVisible(true);

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               List<Student> students = studentDAO.getAllStudents();
                removesAllAndAddNewRows(students);
            }
        });
    }

    public void removesAllAndAddNewRows(List<Student> students){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setColumnIdentifiers(columnNames);
        model.setRowCount(0);
        model.setRowCount(students.size());
        for (int i = 0; i < students.size(); i++) {
            model.insertRow(i+1, new Object[]{students.get(i).getFirstName(), students.get(i).getLastName(),
                    studentDAO.getCoursesByStudentID(students.get(i).getStudentID()), students.get(i).getGradeYear(), 000});
        }
    }




}
