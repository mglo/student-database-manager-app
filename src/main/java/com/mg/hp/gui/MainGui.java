package com.mg.hp.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainGui {

    public static void main(String[] args) {
        //NEW STARTING POINT
        JFrame frame = new JFrame("University Recruitment Manager");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();

        JLabel label = new JLabel("Welcome!     ", SwingConstants.CENTER);
        JButton buttonAddStudent = new JButton("Add Student");
        JButton buttonRemoveStudent = new JButton("Remove Student");
        JButton buttonShowStudents = new JButton("View Students");
        JButton buttonShowCourses = new JButton("View Courses");
        JButton buttonPayments = new JButton("Payments");

        panel.add(label);
        panel.add(buttonAddStudent);
        panel.add(buttonRemoveStudent);
        panel.add(buttonShowStudents);
        panel.add(buttonShowCourses);
        panel.add(buttonPayments);

        frame.getContentPane().add(panel, BorderLayout.CENTER);

        frame.setSize(190,400);
        frame.setVisible(true);

        buttonAddStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddNewStudentGui addNewStudentGui = new AddNewStudentGui();
            }
        });

        buttonRemoveStudent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                RemoveStudentGui removeStudentGui = new RemoveStudentGui();
            }
        });

        buttonShowStudents.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AllStudentsGui allStudents = new AllStudentsGui();
            }
        });

        buttonShowCourses.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Show courses");
            }
        });

        buttonPayments.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Payments");
            }
        });




    }
}
