package com.mg.hp.studentDBapp;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.util.Scanner;

public class Student {
    private static final int costOfCourse = 600;

    private int studentID;
    private String firstName;
    private String lastName;
    private String displayName;
    private int gradeYear;
    private String courses = "";
    private int tuitionBalance;
    private static int id = 1000;

    public Student(String firstName, String lastName, int gradeYear) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.displayName = lastName + ", " + firstName;
        this.gradeYear = gradeYear;
    }

    public Student(int studentID, String firstName, String lastName, int gradeYear) {
        this(firstName, lastName, gradeYear);
        this.studentID = studentID;
    }

    //Generate an ID
    private void setStudentID(){
        //Grade Level + ID
        id++;
        this.studentID = gradeYear + id;
    }

    //Enroll in courses
    public void enroll(){
        Scanner scanner = new Scanner(System.in);

        while(true){
            System.out.print("Enter course to enrol (Q to quit): ");
            String course = scanner.nextLine();
            if(!course.equals("Q")){
                courses += "\n " + course;
                tuitionBalance += costOfCourse;
            }else{
                break;
            }
        }
    }

    //View balance
    public void viewBalance(){
        System.out.println("Your balance is: $" + tuitionBalance);
    }

    //Pay Tuition
    public void payTuition(){
        viewBalance();
        Scanner in = new Scanner(System.in);
        System.out.print("Enter your payment: $");
        int payment = in.nextInt();
        this.tuitionBalance -= payment;
        System.out.println("Thank you for your payment or $" + payment);
        viewBalance();
    }

    //Show status
    public String showInfo(){
        return "Name: " + firstName + " " + lastName +
                "\nGrade level: " + gradeYear +
                "\nStudent ID: " + studentID +
                "\nCourses Enrolled:" + courses +
                "\nBalance: $" + tuitionBalance;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getGradeYear() {
        return gradeYear;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getStudentID() {
        return studentID;
    }
}
