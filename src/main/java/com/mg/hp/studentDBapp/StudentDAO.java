package com.mg.hp.studentDBapp;

import com.mg.hp.utils.CredentialsReader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDAO implements StudentDAOInterface {

    public List<Student> getAllStudents() {
        List<Student> students = new ArrayList<>();
        String selectAllStudents = "SELECT * FROM universitydb.tblStudents";

        try(Connection conn = connect()){
            PreparedStatement ps = conn.prepareStatement(selectAllStudents);
            ResultSet rs = ps.executeQuery();
            students = studentsResultSetToList(rs);
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    public Student getStudent(int studentID) {
        List<Student> students = new ArrayList<>();
        String selectStudentByID = "SELECT * FROM universitydb.tblStudents WHERE studentID = ?";

        try(Connection conn = connect()){
            PreparedStatement ps = conn.prepareStatement(selectStudentByID);
            ps.setInt(1, studentID);
            ResultSet resultSet = ps.executeQuery();

            students = studentsResultSetToList(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    return students.get(0);
    }

    public void addStudent(Student student) {
        String insertStudent = "INSERT INTO universitydb.tblStudents (firstName, lastName, DisplayName, gradeYear)" +
                "VALUES (?, ?, ?, ?)";
        try(Connection conn = connect()){
            PreparedStatement ps = conn.prepareStatement(insertStudent, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, student.getFirstName());
            ps.setString(2, student.getLastName());
            ps.setString(3, student.getDisplayName());
            ps.setInt(4, student.getGradeYear());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            int generatedID = -1;

            while(rs.next()){
                generatedID = rs.getInt(1);
            }
            student.setStudentID(generatedID);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeStudent(Student student) {
        String removeStudent = "DELETE FROM universitydb.tblStudents WHERE studentID = ?";
        try(Connection conn = connect()){
            PreparedStatement ps = conn.prepareStatement(removeStudent);
            ps.setInt(1, student.getStudentID());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getCoursesByStudentID(int studentID) {
        String selectCourseByID =
                "SELECT DISTINCT C.courseName, C.coursePrice " +
                        "FROM universitydb.tblStudentCourses SC " +
                        "join tblCourses C on C.courseID = SC.courseID " +
                        "WHERE SC.studentID = ?";
        try(Connection conn = connect()){
            PreparedStatement ps = conn.prepareStatement(selectCourseByID);
            ps.setInt(1, studentID);
            ResultSet resultSet = ps.executeQuery();

            String courseName = "";
            int coursePrice;
            while (resultSet.next()){
                courseName += resultSet.getString("courseName")+ " ";
                coursePrice = resultSet.getInt("coursePrice");

            }
            return courseName;

        }catch (SQLException e){
            e.printStackTrace();
        }
        return null;
    }


    private Connection connect() throws SQLException {
        return DriverManager.getConnection(CredentialsReader.getURL(),CredentialsReader.getUser(),CredentialsReader.getPass());
    }

    private List<Student> studentsResultSetToList(ResultSet resultSet) throws SQLException {
        List<Student> students = new ArrayList<>();

        int studentID;
        String firstName;
        String lastName;
        int gradeYear;
        while(resultSet.next()){
            studentID = resultSet.getInt("studentID");
            firstName = resultSet.getString("firstName");
            lastName = resultSet.getString("lastName");
            gradeYear = resultSet.getInt("gradeYear");
            students.add(new Student(studentID, firstName, lastName, gradeYear));
        }
        return students;
    }

}
