package com.mg.hp.studentDBapp;

import java.util.List;

public interface StudentDAOInterface {
    List<Student> getAllStudents();
    Student getStudent(int studentID);
    void addStudent(Student student);
    void removeStudent(Student student);
    String getCoursesByStudentID(int studentID);

}
